import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

const game = {
    maxNbrOfPlayers: 3,
    nbrOfDices: 5
}

function FormFields() {
    let elements = [];
    for (let i = 1; i <= game.maxNbrOfPlayers; i++) {
        elements.push(<label>Player {i}: </label>);
        let className = "player" + i;
        elements.push(<input type="text" class={className} />);
        elements.push(<p></p>);
    }
    return elements;
}

function Dices(props) {

    const throwDices = props.throwDices;
    if (throwDices) {
        const players = props.players;
        let elements = [];
        let randomSpotCount = 0;
        let totalSpotCountOfPlayer = 0;
        let bestTotalSpotCount = 0;
        let leader = "";
        for (let i = 1; i < players.length; i++ ) {
            for (let j = 0; j < game.nbrOfDices; j++) {
                randomSpotCount = throwDice();
                totalSpotCountOfPlayer += randomSpotCount;
                let spotCount = require(
                    './images/' + randomSpotCount + '.png');
                elements.push(<img src={spotCount} alt="spotCount" />);
            }
            if (i === 1) {
                leader = players[i];
                bestTotalSpotCount = totalSpotCountOfPlayer;
            }
            else if (totalSpotCountOfPlayer > bestTotalSpotCount) {
                leader = players[i];
                bestTotalSpotCount = totalSpotCountOfPlayer;
            }
            elements.splice(elements.length - game.nbrOfDices, 0,
                <p>Player {players[i]}: {totalSpotCountOfPlayer}</p>);
            totalSpotCountOfPlayer = 0;
        }
        elements.push(<p>Winner is <b>{leader}!</b></p>);
        return elements;
    }
    return null;
}

function throwDice() {
    return Math.floor(Math.random() * 6 + 1);
}

class Game extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            throwDices: false,
            players: []
        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();
        let players = [null];
        for (let i = 1; i <= game.maxNbrOfPlayers; i++ ) {
            let player = event.target.querySelector(".player" + i).value;
            if (player.length > 0) {
                players[i] = player;
            }
        }
        if (players.length > 1) {
            this.setState(state => ({
                throwDices: true,
                players: players
            }));
        }
    }

    render() {
        return(
            <div>
                <h1>Simple dice game</h1>
                <form onSubmit={this.handleSubmit}>
                <FormFields />
                <button>Throw dices</button>
                </form>
                <Dices 
                    throwDices={this.state.throwDices}
                    players={this.state.players}
                />
            </div>
        );
    }
}

ReactDOM.render(<Game />, document.getElementById("root"));